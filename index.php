<?php

namespace index;
require_once 'vendor/autoload.php';

use Class\Functionnality\Test;
//use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


$test = new Test();
$test->saySomething();

$test2 = new \Class\OtherFunctionnality\Test();
$test2->saySomething();

dump('hello');

// create a log channel
$log = new Logger('name');
$log->pushHandler(new StreamHandler('LogFile/MyLog.log', 300));


// add records to the log
$log->warning('Foo');
$log->error('Bar');